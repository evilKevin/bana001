<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row footer">
			<div class="site-footer-inner col-sm-12 remove-padding">

			    <img class="bana-logo-white" src="<?php echo get_stylesheet_directory_uri()?>/images/bana_logo_white.png">
			    <a class="footer-link-twitter" href="https://twitter.com/search?q=%40alabedbana&src=typd" target="blank">@alabedbana</a>
			    <a class="footer-link" href="/terms-of-use/">Terms of use</a>
			    <a class="footer-link" href="/cookies-policy/">Privacy &amp; cookies policy</a>
			    <a class="footer-link" href="/enquiries/">Enquiries</a>
   
				<!-- <div class="site-info">
					<?php do_action( '_tk_credits' ); ?>
					<a href="http://wordpress.org/" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', '_tk' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', '_tk' ), 'WordPress' ); ?></a>
					<span class="sep"> | </span>
                    <a class="credits" rel="nofollow" href="http://themekraft.com/" target="_blank" title="Themes and Plugins developed by Themekraft" alt="Themes and Plugins developed by Themekraft"><?php _e('Themes and Plugins developed by Themekraft.','_tk') ?> </a>
				</div><!-- close .site-info --> -->

			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>
</body>
</html>
