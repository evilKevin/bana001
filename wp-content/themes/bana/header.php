<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
$id = get_the_ID();

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true); ?>
	
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>



<header id="masthead" class="site-header" role="banner">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row nav-banner">
	      	<div class="col-md-12">
	        	<ul class="nav-home">
		          <li><a href="#about">About</a></li>
		          <li>|</li>
		          <li><a href="#book">Book</a></li>
		          <li>|</li>
		          <li><a href="#latest">Latest</a></li>
		          <li><a href="https://twitter.com/search?q=%40alabedbana&src=typd" target="blank"><img class="twitter-logo" src="<?php echo get_stylesheet_directory_uri()?>/images/twitter.png"></a></li>
	        	</ul>
	        	<ul class="nav-misc">
		          <li><a href="/">< Back</a></li>
		          <li><a href="https://twitter.com/search?q=%40alabedbana&src=typd" target="blank"><img class="twitter-logo" src="<?php echo get_stylesheet_directory_uri()?>/images/twitter.png"></a></li>
	        	</ul>
	      	</div>
	    </div>
		<div class="row">
			<div class="site-header-inner col-sm-12">
				
				<img class="hero-image" src="<?php $thumbId = get_post_meta($id, 'homepage_hero_image', true); 
					$image = wp_get_attachment_image_src($thumbId, 'full');
					echo $image[0];
				?>" />  

		
				<div class="bana-logo">
					<?php $header_image = get_header_image();
					if ( ! empty( $header_image ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
						</a>
					<?php } // end if ( ! empty( $header_image ) ) ?>
				</div>


				<div class="site-branding">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<p class="site-description lead"><?php bloginfo( 'description' ); ?></p>
				</div>

			</div>
		</div>
	</div><!-- .container -->
</header><!-- #masthead -->

<div class="main-content">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">

