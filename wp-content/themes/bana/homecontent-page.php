<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _tk
 */
$id = get_the_ID();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">

		<div class="row horizontal hello">	
			<img class="center quote" src="<?php $thumbId = get_post_meta($id, 'section_1', true); 
					$image = wp_get_attachment_image_src($thumbId, 'full');
					echo $image[0];
				?>" />
			<img class="butterfly" src="<?php echo get_stylesheet_directory_uri()?>/images/butterfly.png">
      		<img class="pencils" src="<?php echo get_stylesheet_directory_uri()?>/images/pencils.png">
			
		</div>

		<div id="about" class="row horizontal intro">
			<?php the_content(); ?>
	      <img class="divider" src="<?php echo get_stylesheet_directory_uri()?>/images/divider1.png">
		</div>

		<div id="book" class="row horizontal book">
     		<img class="divider" src="<?php echo get_stylesheet_directory_uri()?>/images/divider2.png">
     		<div class="col-md-6">
				<img class="center" src="<?php $thumbId = get_post_meta($id, 'book_image', true); 
					$image = wp_get_attachment_image_src($thumbId, 'full');
					echo $image[0];
				?>" />
			</div>
			<div class="col-md-6">
				<?php echo get_post_meta($id, 'book_copy', true); ?>
			</div>
		</div>

		<div id="untitled" class="row horizontal untitled">
      		<img class="divider" src="<?php echo get_stylesheet_directory_uri()?>/images/divider_untitled.png">
      		<div class="col-md-6">
				<?php echo get_post_meta($id, 'love_copy', true); ?>
			</div>
			<div class="col-md-6">
				<img class="center" src="<?php $thumbId = get_post_meta($id, 'love_image', true); 
					$image = wp_get_attachment_image_src($thumbId, 'full');
					echo $image[0];
				?>" />
			</div>
		</div>

		<div id="twitter" class="row horizontal twitter">
      		<img class="divider" src="<?php echo get_stylesheet_directory_uri()?>/images/divider3.png">
			<?php echo do_shortcode("[ic_add_posts category='twitter-posts']");?>

		</div>

		<div id="latest" class="row horizontal latest">
      		<img class="divider" src="<?php echo get_stylesheet_directory_uri()?>/images/divider4.png">
			<?php echo do_shortcode('[smartslider3 slider=2]');?>
		</div>

	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', '_tk' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
