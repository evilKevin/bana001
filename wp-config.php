<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bana001');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f`][%Rz6r;~vG3%u]1ho g>8a;dKbJ&6EbMsatr^EcT6-v1U|QXt)n-u)OHd}A,6');
define('SECURE_AUTH_KEY',  'N|?`J-+25p_BZH-pXs+m WpUJ`|-RdERjNn9Yr!a8_RAyyKwe:4yAU?Pj|+qKa-3');
define('LOGGED_IN_KEY',    'q(+J4xx}X+%<16j,wOoh>,}/73`-iEK}1O<mzgR|3toK#G)T!$:|ZL*~]HyN22s!');
define('NONCE_KEY',        'g|#0s|--dDA+f|c.Zq#xxOrTud0u@_1A0eh?zpwK,*o|[[{11Q${=sLtipu~.-W/');
define('AUTH_SALT',        'NBA|?Bj|FmldP-]`^D.^UNJCx+&Hz]]uac[xyTUVOpN2l d%l=y(oG{?o/iclh;N');
define('SECURE_AUTH_SALT', 'aYc(Vw84FCU~S[O3nS,HgJZ(,QpxSoSa+Q{?l/E5/Y+i$)F7Qu^-|f@6!e_c!o6U');
define('LOGGED_IN_SALT',   '<19>;-Do,i_CUX5<F00{qbtw@zMI.X0/:FP|rG38r0`a,-B-!kzzqsZi$ r|G8@+');
define('NONCE_SALT',       'm9*;Hu>x2G/P:.=,x{|$4|h|DH`5X3>.Z+g27n:eJWOm<ev5WSe3n<j|!yp6bj|_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
