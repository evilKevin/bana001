$(document).ready(function(){

  $('.your-class').slick({
	slidesToShow: 1,
	dots: true,
	slidesToScroll: 1,
	autoplay: true,
	speed:500,
	autoplaySpeed: 3000,
	arrows: false,
	focusOnSelect: true,
	accessibility: false
  });


});